# Setup
This README will walk you through how to create a React Native project implementing the ONYX React Native plugin from scratch.

Please follow the React Native documentation for [setting up your development environment](https://reactnative.dev/docs/environment-setup).

# Getting Started
Create a brand new React Native project using the react-native CLI command
```
npx react-native init OnyxReactNativeExample --template react-native-template-typescript
```

# Run On Device
Run the application on a native device to make sure your environment is configured correctly
#### Android
```
npx react-native run-android
```

#### IOS
```bash
npm install -g ios-deploy
react-native run-ios --device "${DEVICE_NAME}’s iPhone"
```

# Telos ID NPM Registry, this writes the repository in to the .npmrc file
```bash
echo @telosid:registry=https://gitlab.com/api/v4/projects/29462567/packages/npm/ >> .npmrc
```

# Install the ONYX React Native plugin
```
npm install @telosid/react-native-plugin-onyx@latest @telosid/onyx-typedefs@latest
```

# Configure the Android project
Edit `android/build.gradle` and copy the Telos ID maven repository into the `allprojects.repositories` section
#### Telos ID Maven Repository
```
  jcenter()
  maven {
    url 'https://gitlab.com/api/v4/projects/29462567/packages/maven'
  }
```
Your `android/build.gradle` `allprojects` section should look like this
````
allprojects {
    repositories {
        mavenCentral()
        mavenLocal()
        maven {
            // All of React Native (JS, Obj-C sources, Android binaries) is installed from npm
            url("$rootDir/../node_modules/react-native/android")
        }
        maven {
            // Android JSC is installed from npm
            url("$rootDir/../node_modules/jsc-android/dist")
        }

        google()
        maven { url 'https://www.jitpack.io' }
        // *** These are the lines that were added
        jcenter()
        maven {
            url 'https://gitlab.com/api/v4/projects/29462567/packages/maven'
        }
        // ***
    }
}
````

Edit `android/app/build.gradle` `android` section to address the `libc++` conflict between React Native and OnyxCamera
```
android {
    /**
     * Note: Add the packagingOptions to select the libc++ lib provided by React Native
     */
    packagingOptions {
        pickFirst '**/*.so'
    }
    ...
```

# Configure the IOS project
Add the OnyxCamera specs repo to your CocoaPods installation
```bash
pod repo add gitlab-telosid-plugins https://gitlab.com/telosid/plugins/specs.git
npx pod-install
```

* Add camera usage description to you `Info.plist`
  * Open `ios/${YourProjectName}/Info.plist` and paste the following key pair just before the closing `</dict>` XML tag
  ```
      <key>NSCameraUsageDescription</key>
      <string>This application will use your phone's camera to capture an image of your fingerprint.</string>
  </dict>
  ```
* Link bundled resources
  * Open the Xcode workspace
```bash
cd ios && open .
```
* Double-click on `${YourProjectName}.xcworkspace`
* Disable `Bitcode`
    * Select the `Build Settings` tab in the main content window
    * Search for `bitcode`
    * Set `Enable Bitcode` to `No` for both debug and release

# Launch ONYX
## Set your ONYX License Key
Create a `.env` file in the root of the project if one does not exist and add your `LICENSE_KEY`
```bash
echo LICENSE_KEY='XXXX-XXXX-XXXX-X-X' >> .env
```

Add the `.env` file to your `.gitignore`
```bash
 echo .env >> .gitignore
```

## Configure your project to use the `.env` file
```bash
npm install react-native-dotenv
```

Add the `react-native-dotenv` plugin to your `babel.config.js`
```js
plugins: [
    [
      'module:react-native-dotenv',
      {
        moduleName: '@env',
        path: '.env',
      },
    ],
  ]
```

## Configure your application to launch ONYX
Replace the contents of `App.tsx` with the following to launch ONYX and display the results
```typescript jsx
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import {
  Button,
  StyleSheet,
  View,
  Image,
  Dimensions,
  Text,
  ScrollView,
  SafeAreaView,
  StatusBar,
  useColorScheme,
} from 'react-native';

// You can create a .env file to hold the LICENSE_KEY
// @ts-ignore
import {LICENSE_KEY} from '@env';
import type {
  IOnyxConfiguration,
  IOnyxPluginResult,
  IOnyxResult,
} from '@telosid/onyx-typedefs';
import {
  FingerprintTemplateType,
  OnyxPluginAction,
  OnyxReticleOrientation,
} from '@telosid/onyx-typedefs';
import {Onyx} from '@telosid/react-native-plugin-onyx';
import {Colors} from 'react-native/Libraries/NewAppScreen';

const onyxConfig: IOnyxConfiguration = {
  action: OnyxPluginAction.CAPTURE,
  licenseKey: LICENSE_KEY,
  returnRawImage: true,
  returnProcessedImage: true,
  returnFingerprintTemplate: FingerprintTemplateType.NONE,
  returnWSQ: true,
  computeNfiqMetrics: true,
  reticleOrientation: OnyxReticleOrientation.LEFT,
};

const App = () => {
  const isDarkMode = useColorScheme() === 'dark';
  // Get device width
  const deviceWidth = Dimensions.get('window').width;

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
    },
    safeAreaContainer: {
      flex: 1,
      paddingTop: StatusBar.currentHeight,
    },
    box: {
      width: 60,
      height: 60,
      marginVertical: 20,
    },
    imageContainer: {
      height: deviceWidth * 0.8,
      width: deviceWidth * 0.8,
    },
    image: {
      flex: 1,
      height: undefined,
      width: undefined,
    },
  });

  const [result, setResult] = React.useState<IOnyxResult[] | undefined>();
  const launchOnyx = async () => {
    console.log('Onyx license: ' + LICENSE_KEY);
    try {
      const onyxPluginResult: IOnyxPluginResult = await Onyx.exec(onyxConfig);
      console.log('result = ', onyxPluginResult);
      setResult(onyxPluginResult.onyxResults);
    } catch (err) {
      console.error('OnyxError: ' + err);
    }
  };

  return result ? (
          <SafeAreaView style={styles.safeAreaContainer}>
            <ScrollView>
              {result.map((onyxResult: IOnyxResult, i: number) => {
                return (
                        <View
                                key={`onyxResult${i}`}
                                style={{
                                  padding: 10,
                                  alignItems: 'center',
                                }}>
                          <Text
                                  style={{
                                    // @ts-ignore
                                    color: isDarkMode ? 'white' : 'black',
                                  }}>
                            {`Fingerprint ${i + 1}`}
                          </Text>
                          <Image
                                  source={{uri: onyxResult.processedFingerprintDataUri}}
                                  style={styles.imageContainer}
                          />
                          <Text
                                  style={{
                                    // @ts-ignore
                                    color: isDarkMode ? 'white' : 'black',
                                  }}>
                            {`NFIQ: ${
                                    onyxResult.captureMetrics?.nfiqMetrics?.nfiqScore ||
                                    'CaptureMetrics not available'
                            }`}
                          </Text>
                        </View>
                );
              })}
              <Button onPress={() => setResult(undefined)} title="Done" />
            </ScrollView>
          </SafeAreaView>
  ) : (
          <View style={styles.container}>
            <Button onPress={launchOnyx} title="Launch ONYX" />
          </View>
  );
};

export default App;


```

### Restart your application
Make sure to kill your Dev server if it is still running and restart your application
#### Android
```bash
npx react-native run-android
```
#### IOS
```bash
react-native run-ios --device "${DEVICE_NAME}’s iPhone"
```
